﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;

namespace Treehouse.FitnessFrog
{
    //Startup Class a Partial Class
    //C# partial class, is a class whose definition is spread across one or more code files. 
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //we need to call it from the Configuration method in the "Startup.cs" file
            ConfigureAuth(app);
        }
    }
}