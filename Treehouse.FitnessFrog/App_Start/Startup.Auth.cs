﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace Treehouse.FitnessFrog
{
    public partial class Startup
    {
        //accepts a parameter of type IAppBuilder
        private void ConfigureAuth(IAppBuilder app)
        {
            //Configuring the Identity Cookie Authentication Middleware Component
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/SignIn"),
                Provider = new CookieAuthenticationProvider()
            });

        }
    }
}

//Now we need to call it from the Configuration method in the "Startup.cs" file in the root of the project.